﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Cortage<T>
{
    Dictionary<int, Dictionary<int, T>> sity;

    public Cortage()
    {
        sity = new Dictionary<int, Dictionary<int, T>>();
        Size = 0;
        Lenght = 0;
    }

    public void Add(Char key1, Char key2, T value)
    {
        if (!sity.ContainsKey(GetIndexKey(key1))) sity.Add(GetIndexKey(key1), new Dictionary<int, T>());
        sity[GetIndexKey(key1)].Add(GetIndexKey(key2), value);

        if (!sity.ContainsKey(GetIndexKey(key2))) sity.Add(GetIndexKey(key2), new Dictionary<int, T>());
        sity[GetIndexKey(key2)].Add(GetIndexKey(key1), value);

        SetLenght(GetIndexKey(key1));
        SetLenght(GetIndexKey(key2));
        Size++;
    }
    public int GetIndexKey(Char key)
    {
        return key - 65;
    }
    public Char GetKey(int index)
    {
        return (Char)(index + 65);
    }
    public Object GetValue(int indexKey1, int indexKey2)
    {
        return (sity.ContainsKey(indexKey1) && sity[indexKey1].ContainsKey(indexKey2)) ? (Object)sity[indexKey1][indexKey2] : -1;
    }
    public int Size { get; private set; }
    private void SetLenght(int index)
    {
        if (index > Lenght) Lenght = index + 1;
    }
    public int Lenght { get; private set; }

}

namespace Tuples
{
    class Program
    {
        static Cortage<int> cortage = new Cortage<int>();
        static void Func1()
        {
            for (int i = 0; i < cortage.Lenght; ++i)
                for (int j = i + 1; j <= cortage.Lenght; ++j)
                    if ((int)cortage.GetValue(i, j) > 0) Console.WriteLine(cortage.GetKey(i).ToString() + " " + cortage.GetKey(j) + " " + cortage.GetValue(i, j));
        }
        static int Func2()
        {
            int minValue = 10000;
            for (int i = 0; i < cortage.Lenght; ++i)
                for (int j = i + 1; j <= cortage.Lenght; ++j)
                    if ((int)cortage.GetValue(i, j) > 0 && (int) cortage.GetValue(i, j) < minValue) minValue = (int)cortage.GetValue(i, j);
            return minValue;
        }
        static int Func3()
        {
            int maxValue = -1;
            for (int i = 0; i < cortage.Lenght; ++i)
                for (int j = i + 1; j <= cortage.Lenght; ++j)
                    if ((int)cortage.GetValue(i, j) > 0 && (int)cortage.GetValue(i, j) > maxValue) maxValue = (int)cortage.GetValue(i, j);
            return maxValue;
        }
        static int Func4()
        {
            int count = 0;
            for (int i = 0; i < cortage.Lenght; ++i)
                for (int j = i + 1; j <= cortage.Lenght; ++j)
                    if ((int)cortage.GetValue(i, j) > 0) count += (int)cortage.GetValue(i, j);
            return count;
        }
        static int Func5(Char key1, Char key2)
        {
            int size = cortage.Lenght;
            int[,] matrix = new int[size, size];
            int[] weight = new int[size];
            int[] visited = new int[size];
            int temp;
            int minindex, min;
            for (int i = 0; i < size; i++)
            {
                matrix[i, i] = 0;
                for (int j = i + 1; j < size; j++)
                {
                    temp = ((int)cortage.GetValue(i, j) > -1) ? (int)cortage.GetValue(i, j) : 0;
                    matrix[i, j] = temp;
                    matrix[j, i] = temp;
                }
            }
            for (int i = 0; i < size; i++)
            {
                weight[i] = 10000;
                visited[i] = 0;
            }
            weight[0] = 0;
            do
            {
                minindex = 10000;
                min = 10000;
                for (int i = 0; i < size; i++)
                {
                    if ((visited[i] == 0) && (weight[i] < min))
                    { 
                        min = weight[i];
                        minindex = i;
                    }
                }
                if (minindex != 10000)
                {
                    for (int i = 0; i < size; i++)
                    {
                        if (matrix[minindex, i] > 0)
                        {
                            temp = min + matrix[minindex, i];
                            if (temp < weight[i])
                            {
                                weight[i] = temp;
                            }
                        }
                    }
                    visited[minindex] = 1;
                }
            } while (minindex < 10000);
            return weight[Math.Abs(cortage.GetIndexKey(key2) - cortage.GetIndexKey(key1))];
        }
        static void Func6(Char key)
        {
            for (int i = 0; i < cortage.Lenght; ++i)
            {
                if (cortage.GetKey(i) != key) Console.WriteLine(key.ToString() + " -> " + cortage.GetKey(i) + " " + Func5(key, cortage.GetKey(i)));
            }
        }

        static void Main(string[] args)
        {
            cortage.Add('A', 'B', 5);
            cortage.Add('B', 'E', 9);
            cortage.Add('C', 'E', 8);
            cortage.Add('C', 'D', 7);
            cortage.Add('D', 'E', 6);
            cortage.Add('A', 'D', 9);

            Console.WriteLine("----Function1----"); Func1();
            Console.WriteLine("----Function2----\n" + Func2());
            Console.WriteLine("----Function3----\n" + Func3());
            Console.WriteLine("----Function4----\n" + Func4());

            Console.WriteLine("----Function5----");
            Console.WriteLine("Откуда: "); string key1 = Console.ReadLine();
            Console.WriteLine("Куда: "); string key2 = Console.ReadLine();
            Console.WriteLine(Func5(key1[0], key2[0]));

            Console.WriteLine("----Function6----");
            Console.WriteLine("Начальная точка: ");
            key1 = Console.ReadLine();
            Func6(key1[0]);


            Console.ReadKey();
        }
    }
}
